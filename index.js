const { Builder, By } = require('selenium-webdriver');

(async function example() {
  // отваря хром
  let driver = await new Builder().forBrowser('chrome').build();
  try {
    // отваря сакта
    await driver.get('https://www.iherb.com/pr/Nature-s-Way-Parsley-Leaf-900-mg-100-Vegan-Capsules/2027');
    // изчаква 1 минута за да затвориш ръчно всички модали за верифициране, бизксвитки и тн
    await driver.wait(new Promise((resolve) => {
      setTimeout(resolve, 60000);
    }));
    // слледващия ред маха бисквитките но ти ще ги махнеш ръчно в началото още
    // await driver.findElement(By.id("truste-consent-button")).click();

    // избира количеството на продукта
    await driver.findElement(By.xpath("//*[@id=\"ddlQty\"]/option[4]")).click();
    // изчаква 1 секунда
    await driver.wait(new Promise((resolve) => {
      setTimeout(resolve, 1000);
    }));
    // добавя в картата
    await driver.findElement(By.name("AddToCart")).click();
    // изчаква 1 секунда
    await driver.wait(new Promise((resolve) => {
      setTimeout(resolve, 1000);
    }));
    // отива на чекаоут страницата
    await driver.get('https://checkout1.iherb.com/cart');
    // въвежда кода за отстъпка
    await driver.findElement(By.id("coupon-input")).sendKeys('wow123');
    // добавя кода
    await driver.findElement(By.id("coupon-apply")).click();
    // изчаква 1 секунда
    await driver.wait(new Promise((resolve) => {
      setTimeout(resolve, 1000);
    }));
    // отива на следващата стъпка
    await driver.findElement(By.css("#proceedToCheckout .css-3jmwx5")).click();
  } catch(err) {
    // пише грешката в кончолата
    console.error(err)
  } finally {
    // изтрий този ред ако искаш да остаивш 
    driver.quit();
  }
})();
